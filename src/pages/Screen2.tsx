import React, {FC} from 'react';
import {Container, Form, Nav, Button} from 'react-bootstrap';
import '../assets/css/pages/screen-2.scss';

const Screen2:FC = () => {
    return (
        <Container className="screen-2">
            <Nav variant="pills" defaultActiveKey="store">
                <Nav.Item>
                    <Nav.Link eventKey="store">Create by store</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="cluster">Create by cluster</Nav.Link>
                </Nav.Item>
            </Nav>
            <Form className="form-screen-2">
                <Form.Group controlId="selectType">
                    <Form.Control as="select">
                        <option>Good Enterprise</option>
                        <option>Bad Enterprise</option>
                        <option>Normal Enterprise</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="title">
                    <Form.Control type="text" placeholder="Title" />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Control as="textarea" rows="5" placeholder="Description" />
                </Form.Group>
                <Form.Group controlId="file">
                    <Form.File
                        label="Upload an Image"
                        custom
                    />
                </Form.Group>
                <Form.Group className="full-width" controlId="dueTime">
                    <Form.Label>Due time (Asia/SG)</Form.Label>
                    <Form.Control value="27 March, 2020 2:07 PM" />
                </Form.Group>
                <Button className="btn-submit" variant="primary" size="lg" block>
                    Submit
                </Button>
            </Form>
        </Container>
    );
};

export default Screen2;

import React, {FC} from 'react';
import {Container, Form, Media, Button} from 'react-bootstrap';
import '../assets/css/pages/screen-1.scss';
import image from '../assets/images/3622110.jpg';

const Screen1: FC = () => {
    return (
        <Container>
            <Form className="form-screen-1">
                <Form.Group controlId="title">
                    <Form.Control type="text" placeholder="Title" />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Control as="textarea" rows="5" placeholder="Description" />
                </Form.Group>
                <Form.Group controlId="createdAt">
                    <Form.Label>Created at</Form.Label>
                    <Form.Control value="27 March, 2020 2:07 PM" />
                </Form.Group>
                <Form.Group controlId="dueBy">
                    <Form.Label>Due by</Form.Label>
                    <Form.Control value="28 March, 2020 2:07 PM" />
                </Form.Group>
                <Form.Group controlId="applicationLocation">
                    <Form.Label>Application Location</Form.Label>
                    <Form.Control value="Cluster 1" />
                </Form.Group>
                <Form.Group controlId="images">
                    <Form.Label className="font-dark">Images</Form.Label>
                    <Media>
                        <img src={image} alt="Image" />
                    </Media>
                </Form.Group>
                <Button className="btn-delete" variant="danger" size="lg" block>
                    Delete
                </Button>
            </Form>
        </Container>
    );
};

export default Screen1;

import React, {FC} from 'react';
import * as RouteName from '../configs/RouteName';
import {NavLink} from "react-router-dom";
import { Container } from 'react-bootstrap';
import '../assets/css/pages/home.scss';

const Home: FC = () => {
    return (
        <Container className="home-page">
            <NavLink
                to={RouteName.SCREEN_1}
                className="btn btn-primary btn-lg btn-block text-uppercase"
            >
                Screen 1
            </NavLink>
            <NavLink
                to={RouteName.SCREEN_2}
                className="btn btn-primary btn-lg btn-block text-uppercase"
            >
                Screen 2
            </NavLink>
        </Container>
    );
};

export default Home;

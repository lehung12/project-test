import React, {FC} from 'react';
import {Route, Switch} from 'react-router-dom';
import * as RouteName from '../configs/RouteName';
import Home from '../pages/Home';
import Screen1 from '../pages/Screen1';
import Screen2 from '../pages/Screen2';

const AppRoute: FC = () => {
    return (
        <Switch>
            <Route exact path={RouteName.HOME} component={Home} />
            <Route exact path={RouteName.SCREEN_1} component={Screen1} />
            <Route exact path={RouteName.SCREEN_2} component={Screen2} />
        </Switch>
    );
};

export default AppRoute;

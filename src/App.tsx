import React, { FC } from 'react';
import Layout from "./layouts";
import AppRoute from "./routes/AppRoute";

const App:FC = () => {
  return (
      <Layout>
        <AppRoute />
      </Layout>
  );
}

export default App;

import React, {FC} from 'react';
import {NavLink} from "react-router-dom";
import * as RouteName from '../configs/RouteName';
import {Nav} from 'react-bootstrap';
import '../assets/css/components.scss';
import '../assets/css/layout.scss';

const Layout: FC = (props) => {
    return (
        <div className="wrapper">
            <Nav className="navbar fixed-top">
                <NavLink to={RouteName.HOME} className="navbar-brand">All custom task</NavLink>
            </Nav>
            {props.children}
        </div>
    );
};

export default Layout;
